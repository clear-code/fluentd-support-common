---
title:     Fluent Package LTSガイド
author:    株式会社クリアコード
date:      2023年9月29日
titlepage: true
toc-title: 目次
toc-own-page: true
header-right: 　 # あえて全角空白で上書き
footer-left: Copyright (C) 2022-2023 株式会社クリアコード
page-background: background.pdf
page-background-opacity: 0.8
---

# Fluent Package LTSガイド

## 本文書の位置づけ

2023年8月からFluentdの新しいパッケージ配布チャンネルとして『Fluent Package LTS（Long Term Support）』をリリースしました。
Fluent Package LTSについて、リリースサイクル、メンテナンスに関する基本情報、企業利用や長期運用のユーザーに対して、新しいリリースサイクルと定期的なメンテナンスで安定運用しやすくなっているポイントをまとめています。
また、従来のパッケージ（td-agent)からの移行についてもご紹介します。

Appendixとして、技術的に詳しい変更点の説明も紹介しています。

# Fluent Package LTSについて

## td-agentの利便性・互換性はそのままにさらに企業で運用しやすいパッケージ
プロジェクトも10年を超え、オープンソースソフトウェアプロジェクトとしてメンテナンスの継続性やユーザーのニーズをあらためて検討し、コミュニティと議論を行いました。
その結果生まれたFluent Package LTS版は事前にアナウンスした期間にわたりセキュリティフィックス及びバグフィックスのみの互換性が保証されたアップデートが提供されるパッケージです。企業における長期の利用においておすすめです。


### Fluent Package LTSと通常版のリリースサイクルイメージ
LTS版では事前にアナウンスした期間にわたりセキュリティフィックス及びバグフィックスのみの互換性が保証されたアップデートが提供され続けます。
![](20230829_fluent-package-scheduled-lifecycle.png)

基本的に、メジャーアップデートが行われると、ひとつ前のバージョンはメンテナンスがされなくなります。

### パッケージのバージョンについて

Fluentdのパッケージは3桁でバージョンを表しています。
![](fluentd-version-number.png)


* 1桁目 メジャーアップデート：1~2年おきにLTS版が新たなサポート期間でリリースされますが、そのタイミングでパッケージのバージョン番号一桁目が変わります。通常版もそれにともないメジャーバージョンアップします。Fluentdのバージョン更新/新規機能追加を含みます。
* 2桁目　マイナーアップデート: 通常版はFluentdのバージョン更新/新規機能追加のために3~6か月に一度変更されます。LTS版にマイナーアップデートはありません。
* 3桁目　セキュリティフィックス・バグフィックス：脆弱性対応/不具合の修正


## Fluent Package LTSとFluent Package 通常版(旧：td-agent)の違い

|                       |  リリーススケジュール    |      　　更新のしやすさ    |
|-----------------------|-----------------------|-------------------------|
| 通常版 | 不定期(年に2~3回)| セキュリティフィックス・バグフィックス時にマイナーアップデートを求められる場合がある。 |
| LTS | サポート期間の事前アナウンス、長期(およそ2年 ごと) | セキュリティフィックス・バグフィックスを目的とした更新が容易。メジャーアップデートには計画的に準備が可能。 |

2023年8月リリース時点で、Fluent Package LTS及び通常版はどちらも v5.0.1となっています。

2023年11月ごろにセキュリティフィックスとバグフィックスが入るため、LTSはv5.0.2になる予定です。その後セキュリティフィクスのたびに3桁目が更新されます。
一方で11月ごろには、Fluentd v1.17.0を同梱した、通常版v5.1.0になる予定です。その後セキュリティフィクスのたびに3桁目が更新されます。


## Fluent Package LTSにマイグレーションするメリット

Fluent Package LTSに移行することで、次のメリットがあります。

* 長期間に渡って安心してアップデートを行えます。
* サポート期間が事前にアナウンスされるので、メジャーアップデートを計画的に準備できます。

LTS版にすることで、Fluentdを利用しているユーザーがもつ「脆弱性対応やバグ修正を反映するためにアップデートしたいが、アップデートによる不具合の発生が心配で簡単にはアップデートできない」という悩みを解決できます。
長期間に渡ってバグフィックスとセキュリティフィックスのみが適用されるので、不具合を気にすることなく容易にアップデートできます。

さらに、メジャーバージョンが上がる際のアップデートについても事前アナウンスがあるために、影響のある機能や変更点について計画的に準備をすることができ、移行で起こりうる問題をさけて、最適な環境を構築できます。


## td-agent v4からFluent Package v5への移行

Fluent Package v5はtd-agent v4の後継パッケージとして設計されているので、スムーズに移行することが可能です。
環境に応じて、[公式ドキュメントのインストール方法](https://docs.fluentd.org/installation)の通りにインストールすることで、移行を行うことができます。

パッケージの構成が変わったことで多くのファイルパスが変化しますが、可能な範囲でアップグレード時に自動でケアされます。
ただし、場合によっては手動のマイグレート作業が必要になる場合があります。
パッケージの構成の変更点や、マイグレート作業が必要な具体的なケースについては、Appendixをご覧ください。


# 今回の更新に関する主要な技術的変更情報

Fluent Package v5.0.0 および v5.0.1 の主な変更点について紹介します。
詳しくはAppendixをご覧ください。

## in_tail: follow_inodesの不具合改善

`in_tail`の[follow_inodes](https://docs.fluentd.org/input/tail#follow_inodes)オプションは、「ローテート後にファイルを収集しなくなる」などの不具合が以前から報告されていました。
今回実装の問題点がいくつか明らかになり、それらの問題を修正しました。

`follow_inodes`オプションは強力な機能ですので、ぜひご利用を検討いただければと思います。

以下、`follow_inodes`オプションの使いどころを紹介します。

`follow_inodes`オプションを利用することで、収集対象ファイルのパスがローテートによって変化しても、inodeを見て追跡を続けることができるようになります。
そのため、ワイルドカード`*`を使ってローテートファイルも含めて収集対象にまとめて指定することが可能になります。

このオプションを利用しない場合は、カレントファイルのみを収集対象にする必要があります。
ローテートファイルまで含めてしまうと、ローテートしたファイルを新しいファイルだと誤認識して、重複してログを収集してしまう可能性があるからです。

常にFluentdが動作し続けている状態であれば、カレントファイルのみを収集し続けるだけで問題ありません。
しかしFluentdが停止している間にローテートが発生すると、ローテート済みファイルに未収集のログが残っている状態になります。
カレントファイルのみを収集対象としている場合は、Fluentdの再起動時にこういったログを収集することができません。

`follow_inodes`オプションを利用して、ローテートファイルも含めて収集対象に指定することで、このようなケースでも全てのログを収集できるようになります。

## Rubyの新機能: YJIT

Rubyのバージョンがv3.2.2になったことにより、高速化の機能であるYJITを使えるようになりました。

[enable_jit](https://docs.fluentd.org/deployment/system-config#enable_jit-experimental)設定によって使うことができます。

YJITがサポートされている環境であれば、[enable_jit](https://docs.fluentd.org/deployment/system-config#enable_jit-experimental)設定によってYJITを使うことができます。

処理速度の高速化が必要な環境がございましたら、こちらで検証をすることなどもできますので、ぜひご相談ください。

## 脆弱性対応

2件の脆弱性対応が入っております。

[CVE-2022-34169](https://github.com/sparklemotion/nokogiri/security/advisories/GHSA-qwq9-89rg-ww72)はtd-agentに影響ありません。

一方で[CVE-2023-38697](https://github.com/socketry/protocol-http1/security/advisories/GHSA-6jwc-qr2q-7xwj)は[in_monitor_agent](https://docs.fluentd.org/input/monitor_agent)プラグインが影響を受けております。


# クリアコードのFluent Package LTSサポートサービスについて

Fluentdコミュニティにおいて、メンテナンスを主導しているクリアコードでは、Fluentd/Fluent Bitをエンタープライズ環境において導入/運用されるSIer様、サービス提供事業者様に対して以下のサービスを提供しています。 これまで日本国内において航空会社、官公庁、通信事業者をはじめとした大規模ユーザーに対してサービスを提供してきました。この度、Fluent Package LTSリリースに伴い、新たなサービスを開始します。

### Fluent Package LTS導入・アップデート支援

それぞれのお客様の環境をヒアリングし、LTS導入・アップデート時に必要な注意点や、影響のある変更点などを個別に合わせてサポートします。

### 脆弱性情報共有

LTS版にセキュリティフィックスが入った場合に、その内容や影響範囲についてご連絡します。
最新の脆弱性対応について影響を把握し、アップデートの緊急性の判断などに役立てていただけます。

## サービス提供の方針

クリアコードでは、Fluentdサポートサービスの提供にあたりFluentd/Fluent Bitやプラグインに不具合が見つかった場合、問題を回避する一時的な対応だけ取るのではなく、作成したパッチは開発コミュニティに還元します。これにより「特定のユーザのみ問題が回避できる独自パッチをメンテナンスする」のではなく、「すべてのユーザが問題を解消可能なパッチを作成し本体に取り込まれるため、特定のユーザーだけがメンテナンスコストを負担する必要がない」状態にします。 そのため、サービス提供にあたり作成したパッチやドキュメントはお客様の機密情報が含まない形で公開します。

## サービスに関する問い合わせ

サービスに関するお問い合わせ、お見積りのご依頼はこちらの[お問い合わせフォーム](https://www.clear-code.com/contact/) [^contact] からご連絡ください。

[^contact]: [https://www.clear-code.com/contact/](https://www.clear-code.com/contact/)


# Appendix 

## Fluent Package LTS v5.0.0 および v5.0.1 リリース関連詳細情報

### 概要
* 2023年8月29日にリリースされたFluentdの新しいパッケージです。
* パッケージの名前をtd-agentからfluent-packageに変更しました。
* 梱包しているRubyのバージョンを 2.7.8 から 3.2.2 に更新しました。
* 梱包しているFluentdのバージョンを 1.16.1 から 1.16.2 に更新しました。
* 今回から、通常版とLTS(Long Term Support:長期サポート)版の二つの配布チャンネルでリリースされます。
* td-agent v4 については、[2023年12月末にEOLとする予定](https://www.fluentd.org/blog/schedule-for-td-agent-4-eol)です。

### パッケージの主な変更点

* パッケージ名が`td-agent`から`fluent-package`に変わります。
* `td-agent`コマンドが`fluentd`コマンドに変わります。
  * 古いコマンド名も引き続き使えます。
* `td-agent-gem`コマンドが`fluent-gem`コマンドに変わります。
  * 古いコマンド名も引き続き使えます。
* `/opt/td-agent/`配下にあったインストールファイル群が、`/opt/fluent/`配下に変わります。
  * 例えば実行ファイル群のパスが`/opt/td-agent/bin/`から`/opt/fluent/bin/`に変わります。
* Linux系
  * デフォルトのサービス名が`td-agent`から`fluentd`に変わります。
    * 古いサービス名をそのまま使うことも可能です。
  * デフォルトのログ出力パスが`/var/log/td-agent/td-agent.log`から`/var/log/fluent/fluentd.log`に変わります。
    * `/var/log/td-agent/`は`/var/log/fluent/`を指すシムリンクになるので、以前のパスもそのまま使えます。
  * 設定ファイルパスが`/etc/td-agent/`から`/etc/fluent/`に変わります。
    * `/etc/td-agent/`は`/etc/fluent/`を指すシムリンクになるので、以前のパスもそのまま使えます。
  * ローテート設定ファイルが`/etc/logrotate.d/td-agent`から`/etc/logrotate.d/fluentd`に変わります。
  * RHEL系
    * サービスファイルが`/usr/lib/systemd/system/td-agent.service`から`/usr/lib/systemd/system/fluentd.service`に変わります。
    * 環境変数設定ファイルが`/etc/sysconfig/td-agent`から`/etc/sysconfig/fluentd`に変わります。
  * Debian系
    * サービスファイルが`/lib/systemd/system/td-agent.service`から`/lib/systemd/system/fluentd.service`に変わります。
    * 環境変数設定ファイルが`/etc/default/td-agent`から`/etc/default/fluentd`に変わります。
* Windows
  * インストール後に自動でサービスが起動しなくなりました。
  * デフォルトのログ出力パスが`/opt/td-agent/td-agent.log`から`/opt/fluent/fluentd.log`に変わります。
  * `TD_AGENT_TOPDIR`システム環境変数に変わって`FLUENT_PACKAGE_TOPDIR`システム環境変数が追加されます。
    * `TD_AGENT_TOPDIR`システム環境変数はアップデート前と同じパス(`{インストールパス}\td-agent\`)を指し続けます。
  * `Td-agent Command Prompt`は名前が`Fluent Package Command Prompt`に変わります。


### 詳細

#### 追加された新機能 【YJIT(Yet Another Ruby JIT)】

梱包しているRubyを v2.7.8 から v3.2.2 へアップデートしたことで、RubyのYJIT(Yet Another Ruby JIT)機能を利用可能になりました。
(Windowsはサポートされていません)

本機能を利用することで処理速度の高速化を期待できますが、どの程度高速化するかはソフトウェアや環境に依存する部分が大きく、代償としてメモリ消費が増加する性質があります。
Fluentdについては十分に検証できていないため、デフォルトでは有効にしていません。

YJITがサポートされている環境であれば、enable_jitオプションを有効化することでYJITを使うことができます。

<system>
    enable_jit true
</system>

処理速度の高速化が必要な環境がございましたら、こちらで検証をすることなどもできますので、ぜひご相談ください。

以下、補足です。

Fluentdのenable_jitオプションはtd-agent v4.4.1 (Fluentd v1.15.2) から利用可能であり、RubyのJIT(Just-In-Timeコンパイラー)機能を利用することができます。
YJIT以前は Ruby v2.6.0 で搭載されたMJITを使うことができました。
MJITに代わるJITとしてYJITが Ruby v3.1.0 で実験的に搭載され、 Ruby v3.2.0 で大きく改善されて実用段階になりました。
Fluentdのenable_jitオプションを使うことで、その環境にあったJITを有効化してワーカープロセスを立ち上げるようになります。
(内部ではRubyの--jitオプションを有効にしてワーカープロセスを立ち上げています)。
fluent-package v5.0.0 から、Windows以外の一般的な環境であれば、これによってYJITが有効化されるようになります。

#### 修正された不具合
|                                                                                       修正内容                                                                                      |                      参考リンク                      |           補足          |
|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------|-------------------------|
| `in_tail`: `follow_inodes`が有効の場合に、ローテート時に誤ってファイル追跡を停止してしまうことがある問題を修正                                                                      | [#4208](https://github.com/fluent/fluentd/pull/4208) |                         |
| `in_tail`: `follow_inodes`が有効の場合に、ローテート時に誤ってファイルの収集位置情報を破棄し、ローテート済みファイルを重複して収集してしまうことがある問題を修正                    | [#4237](https://github.com/fluent/fluentd/pull/4237) |                         |
| `in_tail`: `follow_inodes`が有効の場合に、ローテート時に収集対象の更新が漏れて、ファイルハンドルがリークすることがある問題を修正                                                    | [#4239](https://github.com/fluent/fluentd/pull/4239) |                         |
| `in_tail`: `follow_inodes`が有効の場合に、一部の警告ログが誤って出力されていた問題を修正                                                                                            | [#4214](https://github.com/fluent/fluentd/pull/4214) |                         |
| [ignore_same_log_interval](https://docs.fluentd.org/deployment/logging#ignore_same_log_interval)設定をしている場合、Fluentdのロガーのキャッシュが制限なく増加できてしまう問題を修正 | [#4229](https://github.com/fluent/fluentd/pull/4229) |                         |
| `in_forward`: 壊れたデータを受信した場合、その後に受信するデータが続いて壊れることがある問題を修正                                                                                  | [#4178](https://github.com/fluent/fluentd/pull/4178) |                         |
| Windows: Fluentdのログのローテート設定をしている場合に、ログファイルパスを指定せずに起動するとエラーで起動できない問題を修正                                                        | [#4188](https://github.com/fluent/fluentd/pull/4188) | td-agent v4.5.0のみ発生 |
 
#### 取り込まれた脆弱性修正
| ライブラリー/コンポーネント |                                                脆弱性                                                |
|-----------------------------|------------------------------------------------------------------------------------------------------|
| Nokogiri                    | [CVE-2022-34169](https://github.com/sparklemotion/nokogiri/security/advisories/GHSA-qwq9-89rg-ww72)  |
| async-http                  | [CVE-2023-38697](https://github.com/socketry/protocol-http1/security/advisories/GHSA-6jwc-qr2q-7xwj) |

## 移行に関連して注意が必要なケース

### 手動インストールしているプラグインを利用している場合

基本的には[Upgrade to fluent-package v5](https://www.fluentd.org/blog/upgrade-td-agent-v4-to-v5)に記載の通り、手動インストールしているプラグインの再インストールが必要となります。

### Fluentdのログファイルを監視している場合

Fluentd自身のログファイルの出力パスが変化するため、もしin_tailなどでFluentd自身のログファイルを監視している場合は、新しいパスに設定を更新する必要があります。

### Linux環境

環境変数設定ファイルまたはローテート設定ファイルをカスタマイズされていた場合は、それぞれマイグレートが必要です。
それぞれ新しいファイルに再度設定をしてください。
また、ユーザー定義サービスファイルを使用している場合は、サービスファイルのマイグレートが必要です。

### Windows環境

Windows環境においては、サービス起動時のコマンドラインオプションを確認する必要があります。
管理者権限のPowerShellで次を実行して、fluentdoptの行をメモしておきます。

```
$ REG QUERY HKLM\System\CurrentControlSet\Services\fluentdwinsvc
```
例: `fluentdopt    REG_SZ    -c C:/opt/td-agent/etc/td-agent/td-agent.conf -o C:/opt/td-agent/td-agent.log`
アップグレード後は、こちらが次のように上書きされます。
```
fluentdopt REG_SZ -c C:\opt\fluent\etc\fluent\fluentd.conf -o C:\opt\fluent\fluentd.log
```
もしコマンドラインオプションをカスタマイズしていた場合は、アップグレード後に再度設定をしてください。
管理者権限の`Fluent Package Command Prompt`で次のように`fluentd --reg-winsvc-fluentdopt`コマンドを実行することで、コマンドラインオプションを上書き変更することができます。
```
$ fluentd --reg-winsvc-fluentdopt "-c C:\opt\fluent\etc\fluent\fluentd.conf -o ..."
```
ただしローテート設定`(--log-rotate-age`および`--log-rotate-size)`は、td-agent v4.4.2以降は[system設定](https://docs.fluentd.org/deployment/logging#log-rotation-setting)で行えます。
ローテート設定に関するコマンドラインオプションを設定されていた場合は、system設定に同様の設定を追加してください。
