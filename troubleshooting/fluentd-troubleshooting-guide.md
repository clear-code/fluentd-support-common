---
title:     Fluentdトラブルシューティングガイド
author:    株式会社クリアコード
date:      2023年8月29日
titlepage: true
toc-title: 目次
toc-own-page: true
header-right: 　 # あえて全角空白で上書き
footer-left: Copyright (C) 2022-2023 株式会社クリアコード
page-background: background.pdf
page-background-opacity: 0.8
---

# Fluentdトラブルシューティングガイド

## 本文書の位置づけ

クリアコードはFluentdおよびFluent Bitの開発、各種プラグインのメンテナンスを行っています。
Fluentd/Fluent Bitのサポート、導入支援、プラグイン開発を行った経験をもとに、よくある質問をまとめました。

一般的な問題解決のための手順を「トラブルシューティング方法」として説明し、よくある事例については「トラブルシューティング事例集」としてまとめています。

# トラブルシューティング方法

トラブルの原因を調査するためには、再現環境とその再現方法を特定する必要があります。
一般的に次のような手順を踏むことにより、原因をしぼりこみます。

* インストールしているFluentdのバージョンを確認する
* インストールしているプラグインのバージョンを確認する
* Fluentd本体の問題かプラグインの問題かを切り分ける
* 再現可能な最小の設定ファイルを用意する
* 最新版でも再現可能か確認する

## インストールしているFluentdのバージョンを確認する

どの環境で問題が発生しているのかを確認します。
特定の環境やバージョンでのみ再現する不具合の特定に必要です。

* Fluent Package/Fluent Package LTS(fluent-package)の場合
* Treasure Agent(td-agent)の場合

### Fluent Package/Fluent Package LTSの場合

Windowsの場合、インストール済みアプリケーションの一覧から「Fluent Package」を探し、そのバージョンを確認します。

もしくは、Fluent Package Command Promptから`fluentd --version`を実行し、表示されるバージョンを確認します。

```
> fluentd --version
fluent-package 5.0.1 fluentd 1.16.2 (d5685ada81ac89a35a79965f1e94bbe5952a5d3a)
```

Debian/Ubuntu,Red Hat/CentOSの場合、シェルから`/usr/sbin/fluentd --version`を実行し、表示されるバージョンを確認します。

```
$ /usr/sbin/fluentd --version
fluent-package 5.0.1 fluentd 1.16.2 (d5685ada81ac89a35a79965f1e94bbe5952a5d3a)
```

Fluent Packageと同梱しているFluentdのバージョンについては、Fluentdのwikiに[History of bundled fluentd version](https://github.com/fluent/fluentd/wiki/The-chronicle-of-Fluent-Package) [^history-of-fluent-package] としてまとまっています。

[^history-of-fluent-package]: [https://github.com/fluent/fluentd/wiki/The-chronicle-of-Fluent-Package](https://github.com/fluent/fluentd/wiki/The-chronicle-of-Fluent-Package)

### Treasure Agent(td-agent)の場合

Windowsの場合、インストール済みアプリケーションの一覧から「Td-agent」を探し、そのバージョンを確認します。

もしくは、コマンドプロンプトもしくはPowerShellのプロンプトから`td-agent --version`を実行し、表示されるバージョンを確認します。

```
> td-agent --version
td-agent 4.5.1 fluentd 1.16.2 (d5685ada81ac89a35a79965f1e94bbe5952a5d3a)
```

Debian/Ubuntu,Red Hat/CentOSの場合、シェルから`td-agent --version`を実行し、表示されるバージョンを確認します。

```
$ /usr/sbin/td-agent --version
td-agent 4.5.1 fluentd 1.16.2 (d5685ada81ac89a35a79965f1e94bbe5952a5d3a)
```

TD Agentと同梱しているFluentdのバージョンについては、Fluentdのwikiに[History of bundled fluentd version](https://github.com/fluent/fluentd/wiki/The-chronicle-of-TD-Agent) [^history-of-td-agent] としてまとまっています。

[^history-of-td-agent]: [https://github.com/fluent/fluentd/wiki/The-chronicle-of-TD-Agent](https://github.com/fluent/fluentd/wiki/The-chronicle-of-TD-Agent)

## インストールしているプラグインのバージョンを確認する

Fluentdはプラグインと連携して動作しますが、特定のバージョンの組み合わせで問題が発覚することがあります。
次のようにして、インストールしているプラグインのバージョンを確認します。

* Fluent Package/Fluent Package LTS(fluent-package)
* Treasure Agent(td-agent)

### Fluent Package/Fluent Package LTS(fluent-package)の場合

Windowsの場合、`Fluent Package Command Prompt`を起動し、`fluent-gem list`を実行します。

Debian/Ubuntu,Red Hat/CentOSの場合、`/usr/sbin/fluent-gem list`を実行します。

```
...(途中省略)
fluent-config-regexp-type (1.0.0)
fluent-diagtool (1.0.1)
fluent-logger (0.9.0)
fluent-plugin-calyptia-monitoring (0.1.3)
fluent-plugin-elasticsearch (5.3.0)
fluent-plugin-flowcounter-simple (0.1.0)
fluent-plugin-kafka (0.19.0)
fluent-plugin-metrics-cmetrics (0.1.2)
fluent-plugin-opensearch (1.1.3)
fluent-plugin-parser-winevt_xml (0.2.6)
fluent-plugin-prometheus (2.1.0)
fluent-plugin-prometheus_pushgateway (0.1.1)
fluent-plugin-record-modifier (2.1.0)
fluent-plugin-rewrite-tag-filter (2.4.0)
fluent-plugin-s3 (1.7.2)
fluent-plugin-sd-dns (0.1.0)
fluent-plugin-td (1.2.0)
fluent-plugin-webhdfs (1.5.0)
fluent-plugin-windows-eventlog (0.8.3)
fluent-plugin-windows-exporter (1.0.0)
fluentd (1.16.2)
...
```

### Treasure Agent(td-agent)の場合

Windowsの場合、`Td-agent Command Prompt`を起動し、`td-agent-gem list`を実行します。

Debian/Ubuntu,Red Hat/CentOSの場合、`/sbin/td-agent-gem list`を実行します。

```
...(途中省略)
fluent-config-regexp-type (1.0.0)
fluent-diagtool (1.0.1)
fluent-logger (0.9.0)
fluent-plugin-calyptia-monitoring (0.1.3)
fluent-plugin-elasticsearch (5.3.0)
fluent-plugin-flowcounter-simple (0.1.0)
fluent-plugin-kafka (0.19.0)
fluent-plugin-metrics-cmetrics (0.1.2)
fluent-plugin-opensearch (1.1.3)
fluent-plugin-prometheus (2.1.0)
fluent-plugin-prometheus_pushgateway (0.1.1)
fluent-plugin-record-modifier (2.1.1)
fluent-plugin-rewrite-tag-filter (2.4.0)
fluent-plugin-s3 (1.7.2)
fluent-plugin-sd-dns (0.1.0)
fluent-plugin-systemd (1.0.5)
fluent-plugin-td (1.2.0)
fluent-plugin-utmpx (0.5.0)
fluent-plugin-webhdfs (1.5.0)
fluentd (1.16.2)
...
```

## Fluentd本体の問題かプラグインの問題かを切り分ける

Fluentdの特性上、実運用ではさまざまなプラグインを併用することがあります。
しかし、実運用そのままの設定ファイルをもとに追試するのはたいてい困難を伴います。

最低限Fluentd本体の問題なのか、プラグイン固有の問題なのかを切り分けるのが定石です。
切り分けるには次のような手順を踏みます。

* 設定ファイルをもとに、使用されているプラグインをリストアップする
* 標準でFluentdに同梱されているプラグインのみで再現するか確認する
* 再現しない場合、特定のプラグインに置き換え再現するか確認する

過去の事例では、`fluent-plugin-systemd`がリロードされるたびにメモリリークを引き起こす問題が見つかったことがありました。

* [Plug a memory leaks on every reload](https://github.com/fluent-plugin-systemd/fluent-plugin-systemd/pull/91) [^systemd-plugin-memory-leak]

[^systemd-plugin-memory-leak]: [https://github.com/fluent-plugin-systemd/fluent-plugin-systemd/pull/91](https://github.com/fluent-plugin-systemd/fluent-plugin-systemd/pull/91)

この問題の特定でも、必要なプラグインのみにしぼり、再現条件を特定するのが有効でした。

## 最新版でも再現可能か確認する

タイミングによっては、問題がすでに修正済みだが、まだリリースされてないということがありえます。

次のようにして、最新版のFluentdをインストールして、それでも再現するかどうかを確認するのも定石の1つです。

Windowsの場合、次のようにします。(最新版のバージョンは実行時によって変わります)

```
> git clone https://github.com/fluent/fluentd.git
> cd fluentd
> bundle install
> rake build
> fluent-gem install ./pkg/fluentd-1.16.2.gem
```

Debian/Ubuntu,Red Hat/CentOSの場合、次のようにします。(最新版のバージョンは実行時によって変わります)

```
$ git clone https://github.com/fluent/fluentd.git
$ cd fluentd
$ bundle install
$ rake build
$ sudo fluent-gem install ./pkg/fluentd-1.16.2.gem
```

注: td-agentの場合、gemをインストールするときに `td-agent-gem install ./pkg/fluentd-1.16.2.gem`を実行します。

## 再現可能な最小の設定ファイルを用意する

特定のオプションを有効・無効にしたときだけ、問題が発生するといったこともありえます。
問題の切り分けのためには、可能な限り再現可能な最小限の手順を確立する必要があります。

* `<source>` を必要なもののみに限定する
*  不要なフィルタ `<filter>` ルールを削除する
*  不要なマッチ `<filter>` ルールを削除する
* `<buffer>` をメモリバッファとファイルバッファで切り替えて現象に変化がみられるか確認する
* 不要なプラグインを使っている部分を削除する(どのプラグインが問題を引き起こしているか確認する)

## デバッグログを採取する

Fluentdでは次のログレベルの出力がサポートされています。既定のログレベルは`info`です。

* `fatal`
* `error`
* `warn`
* `info`
* `debug`
* `trace`

不具合の特定には、詳細なログを必要とします。

コマンドラインで特定のログを採取するには次のオプションを指定してfluendを起動します。

* `fluentd -v`
* `fluentd -vv`

`fluentd -v` はデバッグログを出力します。
より詳細なログを取得するには`fluentd -vv` を指定してトレースログを出力します。
コマンドラインから指定できるログレベルはこの2つのみです。

ログレベルは、設定ファイル経由で指定することもできます。

```
<system>
  log_level error
</system>
```

詳細は、[Logging (https://docs.fluentd.org/deployment/logging)](https://docs.fluentd.org/deployment/logging) を参照してください。

# トラブルシューティング事例集

## 起動に関するトラブル

### 事例: Fluentdの再起動時にヘルスチェックに失敗する

出典: [https://github.com/fluent/fluentd/issues/3539](https://github.com/fluent/fluentd/issues/3539)

Fluentdを再起動する際、バッファのフラッシュやクリーンアップ処理などで時間がかかり、
ヘルスチェック(死活監視)に失敗することがあります。

**対策1**: `skip_refresh_on_startup`を有効にする

`in_tail`プラグインには、`skip_refresh_on_startup`というオプションがあります。
再起動時に監視対象の更新処理のタイミングを遅らせることで、結果的に再起動の処理時間を短縮させます。
再起動の処理時間が短縮できれば、死活監視に失敗しにくくなります。


デメリットとしては、再起動の処理時間を短縮できる一方で、監視対象のログの更新の検知までに時間がかかることです。

**対策2**: `read_bytes_limit_per_second`を調整する

ログサイズが大きい場合など、その読み込み処理にかかりきりになることがあります。
`in_tail`プラグインには、`read_bytes_limit_per_second`というオプションがあります。
読み込み処理を一定に抑えることで、リクエストに応答できない時間が続いてしまう問題を解決します。

## バッファーに関するトラブル

### 事例: Fluentdが数時間後にENOENTを出力する

出典: [https://www.clear-code.com/blog/2021/9/21/fluentd-support.html](https://www.clear-code.com/blog/2021/9/21/fluentd-support.html)

2つのFluentdインスタンスのバッファーのパスがかぶっていることが考えられます。

**対策**: 重複しているバッファーをユニークなものに変更する

Fluentdの設定ファイルから重複しているバッファーのパスがないか探し、ユニークなものに変更する

## 接続に関するトラブル

### 事例: Fluentdの再起動時にEADDRINUSEが発生する

出典: [https://www.clear-code.com/blog/2021/10/7/fluentd-eaddrinuse.html](https://www.clear-code.com/blog/2021/10/7/fluentd-eaddrinuse.html)

Fluentdが多重起動していないか、サーバーのエフェメラルポートの設定ミスが
考えられます。

**対策1**: Fluentdが複数起動しないようにする

```
$ ps -ef |\grep fluentd
```

何らかの理由により、Fluentdの前のプロセスが残っていると、再起動時に後続するプロセスがブロックされます。
起動スクリプトを調整するなどの多重起動防止により解決できます。

**対策2**: エフェメラルポートの設定がかぶらないようにする

通常エフェメラルポートの範囲は32768から60999ですが、環境によってはこの範囲を変更している場合があります。
Fluentdがリッスンしているポートが含まれていないか確認します。

```
%  cat /proc/sys/net/ipv4/ip_local_port_range
32768   60999
```

Fluentdは既定で24224ポートをリッスンしようとするので、もしエフェメラルポートの範囲に含まれているとポートが先に使用済みになる可能性があります。

`/etc/sysctl.conf`を編集して、Fluentdがリッスンするポートが含まれないようにすることで解決します。

```
net.ipv4.ip_local_port_range = 32768 60999
```

# よくある質問

## リリース・アップデートについて

### Fluentdのリリーススケジュールは?

おおむね3ヶ月から6ヶ月に一度のペースで新しいバージョンをクリアコードがリリースしています。

月末にリリースすることが多いですが、ユーザーに影響が大きい不具合が見つかった場合などは、随時修正版をリリースしています。

最新版で不具合がすでに修正済みということもあるので、古いバージョンをお使いの場合は、最新版での検証をおすすめしています。

### Fluent Package (LTS)のリリーススケジュールは?

脆弱性への対応や、不具合対応が必要と判断した場合、クリアコードが随時リリースします。
メジャーバージョンはおよそ1年から2年ごと、セキュリティフィックス等を含むアップデートは3ヶ月から6ヶ月程度を目処にリリース予定です。

### Fluent Packageのリリーススケジュールは?

おおむね3ヶ月から6ヶ月のペースで、その時点のFluentdの最新版を同梱したパッケージをクリアコードがリリースします。
最新のFluentdを同梱する都合上、Fluentdのリリースタイミングによりかわることがあります。

### TD AGENTのリリーススケジュールは?

おおむね3ヶ月に1度のペースで、その時点のFluentdの最新版を同梱したパッケージをクリアコードがリリースしています。

ただし、td-agentは2023年8月にリリースされたv4.5.1が最終リリースとなり、サポートは2023年12月に終了予定です。

* Drop schedule announcement about EOL of Treasure Agent (td-agent) 4
  * [https://www.fluentd.org/blog/schedule-for-td-agent-4-eol](https://www.fluentd.org/blog/schedule-for-td-agent-4-eol)

後継となる`fluent-package`へのアップグレード手順については、次の記事を参照してください。

* Upgrade to fluent-package v5
  * [https://www.fluentd.org/blog/upgrade-td-agent-v4-to-v5](https://www.fluentd.org/blog/upgrade-td-agent-v4-to-v5)

### Fluent PackageとFluent Package (LTS)の違いはなんですか?

Fluent Package (LTS)は企業利用を想定し、長期のサポートを提供します。
Fluent Package (LTS) v5.0.xではFluentdを1.16.x系を採用し、セキュリティフィックスおよび不具合修正のみを提供します。
これにより安定した運用ができるようになり、今後のバージョンアップも計画的に行えるため、企業利用におすすめです。

一方のFluent Packageでは、より新しいFluentdを随時採用するので、新機能を使いたい一般ユーザーにおすすめです。

### Fluent Package (LTS) v5.0.xはいつまでサポートされますか?

v5.0.xは2025年3月末のサポート終了を予定しています。
サポート終了時には、移行先となるv6.0.0 (LTS)をリリース予定です。

<!-- https://www.fluentd.org/blog/fluent-package-scheduled-lifecycle から図を借用 -->

![Fluent PackageおよびLTSのサポートライフサイクル](20230829_fluent-package-scheduled-lifecycle.png)

2023年8月時点の想定されるサポートライフサイクルは図のとおりです。将来的に開発状況によって具体的な時期は変わる可能性があります。

### Fluent Package v5.0.xはいつまでサポートされますか?

通常版は常に最新版のみをサポートしています。
Fluent Package v5.1.0のリリースにともない、v5.0.xのサポートも終了します。
(クリアコードでは、古いバージョンをお使いのお客様向けにも有償サポートを提供しています。)

### Fluentd 1.XXはいつまでサポートされますか?

基本的には、新しいマイナーバージョンがリリースされると、そのまえのマイナーリリースバージョンはサポートされなくなります。

つまり、1.18.0がリリースされると、1.17系はサポートされなくなります。

ただし、Fluent Package (LTS)に同梱されているFluentdのバージョンはLTSのサポート期間に限定してサポートが続けられます。
(クリアコードでは、そういった古いバージョンをお使いのお客様向けにも有償サポートを提供しています。)

### リリースに関する日本語の情報はありますか?

毎リリースごとではありませんが、リリースノートを解説する記事を提供しています。

* [https://www.clear-code.com/blog/2023/4/25/fluentd-v1.16.0-v1.16.1.html](https://www.clear-code.com/blog/2023/4/25/fluentd-v1.16.0-v1.16.1.html)
* [https://www.clear-code.com/blog/2023/1/6/fluentd-v1.15.2-v1.15.3.html](https://www.clear-code.com/blog/2023/1/6/fluentd-v1.15.2-v1.15.3.html)
* [https://www.clear-code.com/blog/2022/8/5/fluentd-v1.15.1.html](https://www.clear-code.com/blog/2022/8/5/fluentd-v1.15.1.html)

またセキュリティの観点から影響のありそうな事項に関しては、個別に記事を提供することもしています。

* [OpenSSL脆弱性のtd-agentへの影響について（CVE-2022-3602・CVE-2022-3786）](https://www.clear-code.com/blog/2022/11/2/td-agent-openssl.html)

[https://www.clear-code.com/blog/fluentd/](https://www.clear-code.com/blog/fluentd/) では、リリースノートだけではなく関連プラグインについてや、カンファレンス発表の内容などFluentd関連の記事を公開しています。

## 不具合に関する情報源

### 不具合はどこを参照するとわかりますか?

Fluentd本体の動作不具合が疑われる場合は、基本的にはGitHubのissueに報告されます。
まずはこちらをご参照ください。

  * バグ報告 [https://github.com/fluent/fluentd/issues](https://github.com/fluent/fluentd/issues)
  * 修正パッチ [https://github.com/fluent/fluentd/pulls](https://github.com/fluent/fluentd/pulls)

GitHubにユーザー登録し、上記fluentdのページをwatchすることで、動きがあった際に通知を受け取ることができます。

すでに修正済みの不具合を時系列で調べるには変更点を記載したCHANGELOG.mdの参照をおすすめします。

  * 変更点まとめ [https://github.com/fluent/fluentd/blob/master/CHANGELOG.md](https://github.com/fluent/fluentd/blob/master/CHANGELOG.md)

## FAQはどこを参照するとよいですか?

FAQ的な問題については公式ドキュメントに記載する場合があります。

  * [https://docs.fluentd.org/](https://docs.fluentd.org/)

このドキュメントの管理もGitHubにて行っています、そちらも合わせてwatchするのをおすすめします。

* バグ報告: [https://github.com/fluent/fluentd-docs-gitbook/issues](https://github.com/fluent/fluentd-docs-gitbook/issues)
* 修正: [https://github.com/fluent/fluentd-docs-gitbook/pulls](https://github.com/fluent/fluentd-docs-gitbook/pulls)

## Fluentdをパッケージでインストールしています、不具合はどこを参照するとよいですか?

Fluentdではなくtd-agentのパッケージとしての問題は以下にて管理していますので、こちらも合わせてご参照下さい。

  * fluent-package 5系/td-agent 4系
    [https://github.com/fluent/fluent-package-builder/issues](https://github.com/fluent/fluent-package-builder/issues)
  * td-agent 3系
    [https://github.com/treasure-data/omnibus-td-agent/issues](https://github.com/treasure-data/omnibus-td-agent/issues)

2022年2月以降td-agent 3系はメンテンナンスされていないため、td-agent 4系への移行をおすすめします。

## 気軽に質問できるコミュニティーなどはありますか?

Fluentdのユーザーフォーラム等でも問題や解決方法について議論されることがありますが、漠然とした質問等も多いためS/N比は低いです。
Fluentd本体の問題であった場合はGitHubのIssueに誘導することが多いため、必ずしも参照する必要はありません。
フォーラム等のURLについては以下をご参照下さい。

  * [https://www.fluentd.org/community](https://www.fluentd.org/community)
    * [GitHub Discussions(英語)](https://github.com/fluent/fluentd/discussions)
    * [Slack(英語)](https://launchpass.com/fluent-all)

コミュニティでは英語でのやりとりが必須なため、日本語で気軽にやりとりできるGitHub Discussionのカテゴリを新たに開設しています。

  * [Q&A (Japanese)](https://github.com/fluent/fluentd/discussions/categories/q-a-japanese)

# Fluentdサポートサービスについて

クリアコードでは、Fluentd/Fluent Bitをエンタープライズ環境において導入/運用されるお客様に対してサービスを提供します。

|サービス名称|サービスの概要|費用概算（※）|想定されるお客様|
|-----|-----|-----|-----|
|Fluent Package LTSサポート|Fluentdの長期サポート版パッケージ Fluent Package LTS向けサポート。LTS導入及びメジャーバージョンアップ時のサポート、セキュリティフィックスなどの情報提供、LTSに関わるお問合せ受付|80万～(環境に依存します)|すでにFluent Package(td-agent)を利用されていて、入替作業をトラブルなく実施し、今後のアップデートもスムーズに行いたいお客様向け|
|Fluentd コンサルティングサポート|開発/運用中のサービスの安定運用と改善のためのサポート契約。|年間480万～|Fluentdを導入したものの、問題が発生している場合や、運用に問題のない程度のエラーなどが発生していて状態を改善したいお客様向け|
|Fluentd インシデントサポート|時間制のサポート契約。障害対応などを含め契約時間内であればどんなお問合せにも対応。|都度お見積|新規で導入したい場合や、すでに運用している環境でのお問い合わせなどとにかくFluentd/パッケージ/環境などに関してサポートが必要なお客様向け|

※　概算費用はあくまでも概算となり、ご要望や、環境などを伺ったうえでお見積を提供させていただきます。[お問い合わせフォーム](https://www.clear-code.com/contact/) からお気軽にご連絡ください。


# Fluentdサポートサービス

## サービス提供の方針

クリアコードでは、Fluentd サポートサービスの提供にあたり Fluentd/Fluent Bit やプラグインに不具合が見つかった場合、問題を回避する一時的な対応だけ取るのではなく、作成したパッチは開発コミュニティに還元します。
これにより「特定のユーザのみ問題が回避できる独自パッチをメンテナンスする」のではなく、「すべてのユーザが問題を解消可能なパッチを作成し本体に取り込むため、特定のユーザーだけ
がメンテナンスコストを負担する必要がない」状態にします。そのため、サービス提供にあたり作成したパッチやドキュメントはお客様の機密情報が含まない形で公開します。


## サービス範囲

Fluentd/Fluent Bitの導入検討から運用中のサポートやコンサルティングまで、開発、サポートの経験豊富なFluentd/Fluent Bitメンテナのエンジニアが対応します。

* 導入検討段階
  * Fluentd/Fluent Bitに関する技術情報の提供
  * 技術者向け勉強会の開催
  * 性能評価の支援
* 設計・開発段階
  * Fluentd/Fluent Bitをつかったシステムの設計支援
  * Fluentd/Fluent Bitプラグインの機能拡張/新規開発
  * Fluentd/Fluent Bit本体およびプラグインのソースコードレベルのサポート
* 運用段階
  * 障害発生時のログ調査、ソースコードレベル調査と回避策やパッチの提供
  * システムを継続的に改善していくことを目的としたコンサルティングサービスの提供
  * バージョンアップ時の対応
  * セキュリティフィックスなどの情報提供


### サポートサービスに関するよくあるご質問

#### 深刻なバグ情報の定期的な共有は可能か？

対応可能です。弊社にてFluentdのリリースを行っていることから、リリースにあわせて、貴社向けに変更点等の
説明会を実施します。またFluentd本体の脆弱性や深刻なバグが見つかった場合に早期にご報告することも可能です。

リリースにおける変更点の解説を以下のサイトで紹介しています。

* Fluentd v1.14.4リリース -- データ格納の改良とmacOSバグの修正
[https://www.clear-code.com/blog/2022/1/6/fluentd-v1.14.4.html](https://www.clear-code.com/blog/2022/1/6/fluentd-v1.14.4.html)

脆弱性の報告への対応事例を以下のサイトで紹介しています。

* Fluentd v1.14.2に関してGitHubセキュリティアドバイザリーを公開しました (GHSA-hwhf-64mh-r662)
[https://www.clear-code.com/blog/2021/10/29/fluentd-ghsa-hwhf-64mh-r662.html](https://www.clear-code.com/blog/2021/10/29/fluentd-ghsa-hwhf-64mh-r662.html)


#### インシデント発生時の解析支援は可能か？

ログの解析、既知のバグ情報の調査、解決策・ワークアラウンドの提示まで対応可能です。
過去のサポート実績の一部について、以下のサイトで事例として紹介しています。

* Fluentdが数時間後にENOENTを出力する
[https://www.clear-code.com/blog/2021/9/21/fluentd-support.html](https://www.clear-code.com/blog/2021/9/21/fluentd-support.html)

* Fluentdの再起動時にEADDRINUSEが発生する
[https://www.clear-code.com/blog/2021/10/7/fluentd-eaddrinuse.html](https://www.clear-code.com/blog/2021/10/7/fluentd-eaddrinuse.html)

#### 専任のサポート担当者を配置可能か？

お客様専任の担当者としてFluentdのメンテナーを配置するなど、お客様の要望に沿って体制を構築します。
Fluentd利用環境を把握した上で即座にサポート対応を実施するため、エンジニア複数名をお客様担当として
専任し支援します。また作業内容に応じてFluentdエンジニアを追加投入します。

## サービスに関する問い合わせ

サービスに関するお問い合わせ、お見積りのご依頼はこちらの[お問い合わせフォーム](https://www.clear-code.com/contact/) [^contact] からご連絡ください。

[^contact]: [https://www.clear-code.com/contact/](https://www.clear-code.com/contact/)
