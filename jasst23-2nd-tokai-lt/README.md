# データ収集ツールFluentdの最新動向

JaSST'23 2nd Tokai LT

## How to show

  % rabbit

## How to install

  % gem install jasst23-2nd-tokai-lt-fluentd

## How to create PDF

  % rake pdf
