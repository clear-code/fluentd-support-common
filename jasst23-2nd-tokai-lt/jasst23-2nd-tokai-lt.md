# データ収集ツール\\nFluentdの最新動向

author
: 福田大司郎

institution
: 株式会社クリアコード

content-source
: JaSST'23 2nd Tokai LT

date
: 2023-12-15

allotted-time
: 5m

theme
: clear-code

# About me

# Fluentdメンテナー

# 株式会社クリアコード\\nの\\nソフトウェアエンジニア

# 株式会社クリアコード

![](images/clearcode.svg){:relative-width="90"}

{:.center}

**自由ソフトウェア**と**ビジネス**を両立！

# Fluentdメンテナー3名！！

![](images/maintainers.png){:relative-height="100"}

# 2021年から\\nコミュニティの\\n中心となって開発に参加

# Fluentdとは？

> Fluentd is an open source data collector that unifies data collection and consumption.

![](images/Fluentd_icon_square.png){:relative-width="20"}

# Fluentdの役割

![](images/fluentd-architecture.png){:relative-width="100"}

## プロパティー

enable-title-on-image
: false

# Fluentdの特徴

# 自由ソフトウェア(オープンソース)

# 1000以上のプラグイン

# 自分でプラグインを作れる(Ruby)

# スケール\\nする

# スケーラビリティ

![](images/scaling.png){:relative-width="75"}

## プロパティー

enable-title-on-image
: false

# 大規模\\n運用可能

# クラウドネイティブと相性◎

# Fluentd\\nのパッケージ

# td-agent

![](https://raw.githubusercontent.com/fluent/fluent-package-builder/v4.5.1/td-agent/msi/assets/icon.png){:relative-width="55"}

# 簡単に\\nインストール

# 歴史

| Year |      Package      | Fluentd |
|------|-------------------|---------|
| 2011 | td-agent v1       | v0.9    |
| 2014 | td-agent v2       | v0.10   |
| 2017 | td-agent v3       | v0.14   |
| 2020 | td-agent v4       | v1.11   |
| 2023 | fluent-package v5 | v1.16   |

# Fluent Package

![](images/Fluentd_icon_square.png){:relative-height="100"}

# td-agentの後継

# Long Term Support

![](images/fluent-package-scheduled-lifecycle.png){:relative-width="100"}

# 最低2年間

# バグFIX\\nセキュリティーFIX

# アップデートしやすい！

# 長期の\\n安定運用に\\nおすすめ

# td-agentからアップグレード\\n可能！

# 最新の\\n変更点

# in_tailプラグイン\\nのバグ修正\\n

# 収集が止まる深刻なバグ

# 11/29にリリースしたFluent Package v5.0.2で修正

# 組み込みRuby\\n2.7 => 3.2

# Fluentdを使って\\nデータを効率的に\\n管理しよう！
