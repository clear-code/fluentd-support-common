# New Chapter of Fluentd

subtitle
: Rebranding and New Release Cycle (LTS)

author
: Daijiro Fukuda, Kentaro Hayashi

institution
: ClearCode Inc.

content-source
: Open Source Summit JAPAN 2023

date
:   2023-12-05

allotted-time
:   35m

theme
: clear-code

## note

Hi everyone!
Thanks for coming to hear our talk.
We are glad to be here!
Let's start!

We gonna talk about New Chapter of Fluentd,
Rebranding and New Release Cycle LTS, stands for long term support.

# About us

* **Maintainers of Fluentd Project**
* Software Engineers at ClearCode Inc
  * Driving forward Free Software and Make the business

![](images/clearcode.svg){:relative-width="40"}

## note

Let me introduce ourselves.
we are maintainers ...
I am Daij... He is Ken....
we are also ...
clearcode is software development company located in Japan.
Clearcode drives ..
I think we are trying something unique.
Anyway, we are participated in ...

# Fluentd and us

![](images/maintainers.png){:relative-height="75"}

{:.center}

{::note}<https://github.com/fluent/fluentd/graphs/contributors?from=2021-01-01&to=2023-11-30&type=c>{:/note}

## note

This graph shows how active ...
3 of 4 top contributors are from clearcode.
We are deeply involved in recent development of fluentd.
///
I am the number 3 daipom, and he is the number 4 kenhys.
Today, number 1 contributor Ashie is not here.
Even though, please be happy with us.

# Agenda

* What is Fluentd
* History of Fluentd
* What's new Fluent Package LTS
* Remarks of updating
* Major changes

## note

Here is the agenda of our talk.

First topic is ... Second topic is ... How it is ...
Third topic is ... We released new package ... We gonna explain it ...
Fourth topic is ... How users can ..., and why users should.
Last but not least, we gonna talk about major changes and the future of Fluentd.

At end of this talk, we will be happy if everyone here understands about ...
and feels encouraged ...
And more people are interested in the Fluentd community.

# Agenda

* **What is Fluentd**
* History of Fluentd
* What's new Fluent Package LTS
* Remarks of updating
* Major changes

## note

Let's start with what is Fluentd.

# What's Fluentd

> Fluentd is an open source data collector that unifies data collection and consumption.

{::comment}
See https://raw.githubusercontent.com/fluent/fluentd-docs-gitbook/1.0/images/logo/Fluentd_icon_square.png
{:/comment}

![](images/Fluentd_icon_square.png){:relative-width="20"}

## note

I am sure everyone here heard about Fluentd.
The need to collect data continues to expand. Fluentd is one of known data collecters.
読む。
said in the official website.
It is used by so many companies.
And also forked over 1300 times in GitHub.
It is very famous product.

# Image of data collection without Fluentd

{::comment}
See https://raw.githubusercontent.com/fluent/fluentd-website/master/public/images/fluentd-before.png
{:/comment}

![](images/fluentd-before.png){:relative-width="55"}

## note

Here is an image ...
In recent use cases, there are many ...
Data sources are like ...
Data comes from many kinds of sources, in different format.
Data outputs are like ...
It requires to be in specific format.
Without fluentd, it will be complex like this.

## Slide properties

enable-title-on-image
: false

# What Fluentd does

{::comment}
See https://raw.githubusercontent.com/fluent/fluentd-website/master/public/images/fluentd-architecture.png
{:/comment}

![](images/fluentd-architecture.png){:relative-width="100"}

## note

But, Fluentd solves it!

Fluentd unifies logging layer and organizes data processing.
Fluentd can do a whole set of things that are important for data processing.
collecting ...
As you can see, data collection becomes very simple with Fluentd.

## Slide properties

enable-title-on-image
: false

# Why use Fluentd?

* Flexibility
* Scalability
* Open source and community

## note

There are 3 remarkable points why use Fluentd.

First, flexibility.
Fluentd has plugin system.
The plugin system unifies many data sources and outputs.

Second, scalability.
It can handle large data collection, and it is well compatible with distributed systems, like cloud native computing.

Third, open source and community.
Anyone can participate in the development of fluentd.

# Flexibility

* Over 1000 plugins for many data sources and outputs
* By combining plugins, you can achieve what you want
  {::note}Main functions such as data collection, filtering, formatting, and sending are performed by each plugin{:/note}
* You can easily make your plugin with Ruby

## note

First, flexibility.
Fluentd has plugin system.
And there are over 1000 plugins for many ...
You can choose and combine them, and you can achieve ...
If you can't find plugin you want, you can easily ...

# Scalability

![](images/scaling.png){:relative-width="75"}

## note

Scalability.

When Fluentd works as forwarder and aggregator in the log collection system.
You can change the number ... depends on how much data to transfer.
How to manage it is all up to what you want.
The number of instances can be dynamically changed,
so this scalability is very important for Fluentd.

## Slide properties

enable-title-on-image
: false

# Open source and community

* You can join to development and make what you want
* Plugins are maintained in the community
  * <https://github.com/fluent-plugins-nursery>

## note

Open source and community.

Fluentd is open source, the license is Apache License 2.0.
you can refer to the implementation, and you can make your plugin.
//// Also, if you find any bugs, you can report it!
Some bugs are solved with community's help.
//// You can join the development of Fluentd.
//// If it necessary, community supports to maintain plugins with nursery project.
So you can get help to maintain your plugins.

# Agenda

* What is Fluentd
* **History of Fluentd**
* What's new Fluent Package LTS
* Remarks of updating
* Major changes

## note

Now, you know what is Fluentd and why you use it.
Next, I gonna tell you the history!

# History of Fluentd

| 2011   | v0.9 Released by Treasure Data   |
| 2016   | Accepted to CNCF                 |
| 2017   | v1.0 Released                    |
| 2019   | Graduated CNCF project           |

## note

Fluentd was born on 2011 at Treasure Data,
US origin company, providing the marketing data platform.

version 1 was released on 2017.
It was the base of the current product.

Fluentd was accepted to Cloud Native Computing Foundation on 2016.
On 2019, it became the Graduated project maturity level.

It was good to have support from CNCF

# Needs of distribution package

* Fluentd is general purpose
* Plugins allow specific features
* Ruby is required to use

Users want **convenience**. We need distribution package.

## note

Now, I gonna explain the history of the package.
Dist pkg of Fluentd has also been developed from early days of ...

// We need a package to use Fluentd easily.
Fluentd doesn't work without ...
Users need to install ...
Also Fluentd needs Ruby environment.

// If there is a package including ..., ... convenient for users.

# History of distribution package

| Year |      Package      | Fluentd |
|------|-------------------|---------|
| 2011 | td-agent v1       | v0.9    |
| 2014 | td-agent v2       | v0.10   |
| 2017 | td-agent v3       | v0.14   |
| 2020 | td-agent v4       | v1.11   |
| 2023 | fluent-package v5 | v1.16   |

## note

So the package of Fluentd has been developed.
Here is the history of distribution package.

// As a package, td-agent was developed by Treasure Data on 2011.
Originally, the package was for Treasure Data's own services.
But it became widely used as a general package of Fluentd.

// As you can see, along with the development of ...
And recently, the development became on the community base.

// And this year, we release Fluent Package as the successor of td-agent.
Why did the name change? and what else has changed?

# Reason of rebranding

* The name td-agent does not associate with Fluentd
  {::note}The name of distribution package is better to associate or describe the main software{:/note}
* td-agent is so called all-in-one package, not limited for Treasure Data CDP

## note

So why we changed the name?
Actually, it is the big change we had made.
The reason of this change is the name of package is better to describe its main software.
And The name td-agent does not associate with Fluentd. It is difficult to imagine it is actually the package of Fluentd.
Also, td-agent is so called all-in-one package, not limited for Treasure Data services.

# Fluent Package

* The new package name reflects community driven development!

{::comment}
See https://raw.githubusercontent.com/fluent/fluentd-docs-gitbook/1.0/images/logo/Fluentd_icon_square.png
{:/comment}

![](images/Fluentd_icon_square.png){:relative-width="35"}

## note

The new package name 'Fluent Package' reflects community driven development!
It is important to make the name describing it includes Fluentd.
So we have discussed with community and made the name  Fluent Package.

It was a big challenge to change the name for developers.
we have worked through to make sure it does not cause too much trouble for users!
/// (No one clapping?! patipatipatipati)
(Thank you, Thank you)
(You know it is not easy to change the package name! We gonna share details later! But it was hard task!)
/// Also //// while changing the name, we discussed with community about the release cycle as well.

# Agenda

* What is Fluentd
* History of Fluentd
* **What's new Fluent Package LTS**
* Remarks of updating
* Major changes

## note

I have introduced what is Fluentd, and history of it.
Now you realize we have changed the name of the package this year.
So, what else has changed?
Not only did we change the name, but also we renewed the release cycle.
That is the Fluent Package LTS!
Now let me share what is New Fluent Package LTS.

# Fluent Package LTS

* v5.0.2 had been released at Nov 2023 🎉
* Recommended for stable production use
  * **Scheduled** support lifecycle
  * Security fixes, bug fixes for long term

## note

LTS means Long Term Support.
The newest version, 5.0.2 had been released at November.
It is Recommended for stable production use.

// While we involved in the development of Fluentd,
there were certain number of request to have longer support cycle.
In the LTS package, we provide scheduled support lifecycle.
In the lifecycle, we provide only bug fixes and security fixes.
It makes it easy to update continuously!

// We believe that this will meet the needs of users.
Users who want to use Fluentd stably over the long term.

# Standard and LTS

|         Channel         | Support lifecycle  |
|-------------------------|--------------------|
| Standard version        | Every 3 months     |
| LTS (Long term support) | Every 2 year or so |

## note

I gonna explain the lifecycles in more detail.
There are 2 channels with different lifecycles, Standard and LTS.

// Standard is the same lifecycle as the td-agent.
It is very simple.
About every 3 months, we update the embedded Fluentd to the latest.
So, you can always use the latest functionalities with Standard channel.

//But some users may feel it is difficult to update.
Because they may worry about encountering new bugs or losing compatibility.

// LTS life cycle is for 2 years or so.
We continue to provide only bug fixes and security fixes.
It means less possibility of encountering new bugs or losing compatibility.
So, it is recommended for stable production use.

# Planned release cycle

{::comment}
Based on https://raw.githubusercontent.com/fluent/fluentd-website/master/public/images/blog/20230829_fluent-package-scheduled-lifecycle.png
Execute `rake chart` to generate image.
{:/comment}

![](images/fluent-package-scheduled-lifecycle.png){:relative-width="100"}

{::note}<https://www.fluentd.org/blog/fluent-package-scheduled-lifecycle>{:/note}

## note

Here is the chart to describe the schedule image.
/// First section describes the schedule of td-agent version 4 series.
It is already announced that td-agent v4 series reaches end-of-life by the end of 2023, this year.
/// The middle part with a white background is Standard channel.
And the bottom part with a yellow background is LTS channel.
/// Currently, both provides same Fluent Package version 5.0.
But, it will change in the future.
/// As you see, in Standard channel, we update the minor version.
We will provide Fluent Package v5.1 in Standard channel, when next minor update of Fluentd 1.17 will be released.
It will be different from LTS.
/// In LTS channel, we conitunes to provide version 5.0 series for at least 2 years.
We update only the patch version, not minor version.
We announce the update schedule in advance.
At the moment, the next major version 6.0 is scheduled to be released on 2025.
And the current version in LTS channel will be supported until then.

# Profit for updating to LTS

* Secure and stable operation!
  {::note}Long Term Support for updates, ready for stable production use{:/note}
* Less hustle for major update!
  {::note}Advanced announcement for scheduled major update{:/note}
  {::note}Next major update is scheduled in April, 2025 (T.B.D.){:/note}

## note

Let's summarize the points of Fluent Package LTS.

First, secure and stable operation!
You can easily continue updating!

Second, less hustle for major update!
We announce the support lifecycle in advance,
It makes major updates more user-friendly!
Users doesn't need to be hustle. We hustle!

# Agenda

* What is Fluentd
* History of Fluentd
* What's new Fluent Package LTS
* **Remarks of updating**
* Major changes

## note

Then, I'm going to take over the rest of presentation.
I'm mainly contributed to the development of fluent-package, so fit to explain about it.

Hearing what he have told, now I'm sure you are ready to use new Fluent Package.
Now we gonna share some points that what need to be remarked for update.
Information would be useful.

# How to update from \\n td-agent v4

* In most cases, you just install Fluent Package! 🎉
  * **Compatible with td-agent v4!**
* In some cases, manual migration is necessary

## note

Many of you might wonder how difficult to update.
In most cases, you just install Fluent Package!
Just install it and it is ready to use it!
See // you can feel how easy it would be to update!

But manual migration is still necessary because td-agent can be customizable path in many ways.
We can't expect the all of migration path, let you manual migration.

# Compatibility

You can still use ...

* your config files as is
  * {::note}/etc/td-agent/td-agent.conf{:/note}
* commands with their old names
  * {::note}td-agent, td-agent-gem{:/note}
* td-agent user and group

## note

Basically, we take care of keeping compatibility, so if you upgrade from td-agent version 4,
you can still use old config files, commands, user name and group.

If you had ever experienced package development, you know the difficulty of these transitional package.
The internal path was totally changed and for providing old command, it need to make symbolic link.
For moving old log files, it need to implement maintainer hook script.
Using maintainer hook script must be carefully implemented not to break package at all.
For usability of old log rotate configuration, need to keep old user name and group.

If you install newly fluent-package, these compatibility layer is not provided.

# Manual migration cases

* General case
* Specific to Linux
* Specific to Windows

{::note}See <https://www.fluentd.org/blog/upgrade-td-agent-v4-to-v5> for details!{:/note}

## note

There are 3 kind of manual migration cases.
general case, for Linux and for Windows.

We already published upgrade guide as fluentd's blog article.

# Manual migration: general

* Manually installed plugins
  * Reinstall those plugins manually
* Setting of monitoring Fluentd's log file
  * Update the filepath
  {::note}/var/log/fluent/fluentd.log{:/note}

## note

In general, if you installed additional plugins, you need to reinstall plugins for fluent-package.
This is because the plugin path of td-agent is not recognized by fluent-package.
Recent version of fluent-diagtool (fluentd diagnostic tool) gem supports to detect only list of
locally installed gems. so it will help to reinstall gems for fluent-package.

And you need to update the filepath settings if you monitor Fluentd's log file.

# Manual migration: Linux

* Environment file
  * {::note}RHEL: /etc/sysconfig/fluentd{:/note}
  * {::note}Debian: /etc/default/fluentd{:/note}
* Log rotate configuration
  * {::note}/etc/logrotate.d/fluentd{:/note}
* Systemd unit file

## note

For linux, you need to care new path of fluent-package,
environment file, log rotate configuration and Systemd unit file.

# Systemd unit file

Update custom unit files (If you use)

{::note}e.g. /etc/systemd/system/td-agent.service{:/note}

* Update Fluentd bin path
  * {::note}/opt/td-agent/bin/fluentd -> /opt/fluent/bin/fluentd{:/note}
* Update environmental variables
  * {::note}LD_PRELOAD: /opt/fluent/lib/libjemalloc.so{:/note}
  * {::note}GEM_HOME, GEM_PATH: /opt/fluentd/lib/ruby/gems/3.2.0/{:/note}

## note

As for Systemd unit file, if you customized it, 
need to update fluentd binary path and some environment variables too.

# Manual migration: Windows

* Command line arguments (If you use)
  * Set them again
    {::note}$ fluentd --reg-winsvc-fluentdopt ...{:/note}
* Automatic start as Windows Service
  * Enable it manually if you need
    {::note}$ sc config fluentdwinsvc start=delayed-auto{:/note}

## note

For Windows, if you customized command line arguments, you need set them again because fluent-package installer reset them.

And note that since fluent-package version 5, fluentd service does not start automatically.
There is no merit to start automatically because you need to shape configuration in advance.

# Fluentd's log rotation setting on Windows

* No longer need to use command line argument, **`--log-rotate-age`**, **`--log-rotate-size`**
  {::note}Since td-agent v4.4.2, you can use system configuration{:/note}
* Use [system](https://docs.fluentd.org/deployment/logging#log-rotation-setting) configuration
  {::note}https://docs.fluentd.org/deployment/logging#log-rotation-setting{:/note}

## note

On Windows, there is one more thing for you.
It is not limited to fluent-package change, since td-agent version 4.4.2, system directive was supported.
So you need not to customize command line arguments for log rotate configuration.
Just use system directive in fluentd configuration.

This should make your system maintenance easier because no need to modify log rotation option via command line.

We have explained remarked topics about general and platform specific ones.

If you had any problem found updating your system, please let us know!
Fluentd community can support you!

# Agenda

* What is Fluentd
* History of Fluentd
* What's new Fluent Package LTS
* Remarks of updating
* **Major changes**

## note

We are going to share technical details of new version.

# Changes for Fluent Package

* Command name
  {::note}If updated from td-agent, you can still use the old name{:/note}
  * td-agent => fluentd, td-agent-gem => fluent-gem
* Linux: systemd unit name
  {::note}You can still use the old name{:/note}
  * td-agent => fluentd
* Windows: no automatic start

## note

These points were covered in previous section.
but let me explain again!

If updated from td-agent version 4, command name was changed to fluentd or fluent-gem.
But you can still use the old command line.

And name of systemd unit file was also changed to fluentd. you can still use td-agent as alias.

On Windows, fluentd service doesn't start automatically.

# Improvements

* Fluentd: Fix **in_tail** bug
  * Many other improvements!
    {::note}See <https://github.com/fluent/fluentd/blob/master/CHANGELOG.md>!{:/note}
* Fluent Package: Update embedded **Ruby** to **3.2**
  {::note}from 2.7{:/note}
  * Many other improvements!
    {::note}See <https://github.com/fluent/fluent-package-builder/blob/master/CHANGELOG.md>!{:/note}

## note

There are many improvements for fluent-package.
Remarkable changes are bug fixes of in_tail in Fluentd and update with bundled Ruby version.

# Fix in_tail bug

![](images/blog-in_tail.png){:relative-width="100"}

{::note}<https://www.fluentd.org/blog/fluentd-v1.16.2-v1.16.3-have-been-released>{:/note}

## note
This is a screenshot of fluentd release note

There was a long standing bug which wrongly stopping tailing in in_tail.
in_tail is used to collect the appended content of log files.
This bug was observed time to time, but hard to fix it.

# Fix in_tail bug

* Finally, the bug is fixed!
* With **collaboration of many users**, the cause was found, and it is fixed! 🎉

## note

That bug was fixed in recent release of Fluentd.

With **collaboration of many users**, the cause was found, and it is fixed!

He - fukuda and Ashie - No.1 contributor of Fluentd done it!

# Fix in_tail bug

![](images/issue4190.png){:relative-width="70"}

## note

This is a screenshot of GitHub issue about this bug.
Feedback from user are very appreciated.

## Slide properties

enable-title-on-image
: false

# Fix in_tail bug

![](images/pr4185.png){:relative-width="70"}

## note

This is a screenshot of suggested fix.

We could fix it with these kind of feedback.
Without it, I don't know when we could fix it.

## Slide properties

enable-title-on-image
: false

# Update embedded Ruby to 3.2

* You can try Ruby's performance improvements **YJIT**!
  {::note}<https://docs.fluentd.org/deployment/system-config#enable_jit-experimental>{:/note}
  * It is still experimental in Fluentd
    {::note}Please try it and give us feedback!{:/note}

## note

As you know, Ruby 2.7 was reached EOL, so fluent-package bundles Ruby 3.2.
ruby 3.2 process faster, performance improvements
you can take advantage of it!

Though it is still experimental in Fluentd, but I'm sure you know, you can try YJIT.
If fluentd developer could collect enough positive feedback, YJIT will be enabled by default in someday.

# Fluent Package and future

* What happened?
* What will happen?

## note

We have explained current work.

And it is important for the community to think about the future.
In order to plan future, understanding the current position is necessary.
So, here are some facts from statistic of Fluentd activities.

From these statistics, we may see what happened and what will happen.

# The fact from statistics (1)

* Recent Top 5 requests:
  * {::wait/}United States (44.74%)
  * {::wait/}Netherlands (9%)
  * {::wait/}**Japan (6.79%)**
  * {::wait/}France (5.96%)
  * {::wait/}Ireland (5.58%)

## note

First, I'll introduce recent Top 5 requests against td-agent/fluent-package repository on AWS.

Most of request comes from United States.

and followed by Netherlands, Japan, France and Ireland.

In Japan, td-agent/fluent-package are used more than we expected.

# The fact from statistics (2)

Recent trends:

* Top 5 location Before LTS release

  * United States,Netherlands,Japan,France,Ireland

* Top 5 location After LTS release

  * United States,Netherlands,Japan,France,**Taiwan**

## note

Comparing between Before LTS release and After LTS release,
the access from Taiwan is gradually increased.

It is curious but I don't know the reason why.
If anyone know the reason, please tell us later.

# The fact from statistics (3)

* The undesirable facts:
  * The majority of td-agent users: 3.x (unmaintained) or 4.x series.
  * There is still an access for 2.x (unmaintained) 

## note

We have found the undesirable facts.

Most of users still use version 3 or version 4.
version 3 are old-old version!

It's surprised because there is still an access for version 2. It's old-old-old stable.

# The fact from statistics (4)

* The most widely used package:
  * td-agent 4.x (aarch64) for Amazon Linux 2.

## note

We found the popular package is td-agent version 4 for Amazon Linux 2.

td-agent package is provided for intel architecture, but maybe with cost merit, arm architecture was selected.

# The fact from statistics (5)

* GitHub Discussions low activity

![](images/github-discussions.png){:relative-height="100"}

## note

Currently, GitHub Discussions has low activity.
Many questions were post, but there is a few number of supporters.

So we want to encourage users to help each other.

# The future

* No more old package users
* Renewal about distribution channel (Sponsored by CNCF?)
* Encourage users to be a fluentd supporter
  {::note}Join GitHub discussions!{:/note}
* Continuous release

## note

How do feel about the fact of statistics?

First one: We introduced LTS for enterprise use, so don't keep using old packages.

Second one: Currently, it aims to more community based development, repository hosting is heavily rely on Treasure Data.
It's hard to owe Fluentd Project about massive traffic, so we contact to CNCF for sponsoring.
CNCF is supportive about it.

Third one: There is difficulty to get help in timely manner in GitHub discussions, please more and more people join it.
When fixing for in_tail bug, many people give us feedback, so it is happy if same thing will happen on GitHub Discussions too.

Last but not least: Release channel of LTS had just started, we want to keep continuous release.

# Wrap up

* Interested in and understand Fluent Package LTS!
* Encouraged switching to fluent-package LTS further more!
* Encouraged joining Fluentd community!

## note

If you feel excited to know Fluent Package LTS and want to use it or want to join Fluentd community,
very appreciated.

Thank you everyone
We'd love to talk afterwards if you have some feedback.
