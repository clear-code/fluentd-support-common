# fluentd-support-common

Fluentdに関する公開資料を管理するためのリポジトリです。

## トラブルシューティングガイド

makeとdockerが実行できる環境であれば、PDFを生成できるようになっています。

```console
$ make
```

エラーがなければ`fluentd-troubleshooting-guide.pdf`が生成されます。

**ローカルでの実行例**

Ubuntu

```console
$ sudo apt install fonts-noto-cjk texlive-fonts-extra
$ cd troubleshooting
$ make local
```

## LTS版紹介スライド

fluent-package v5 LTSのリリースについて紹介するスライドです。

Rubyが実行できる環境であれば、スライド表示やPDF生成を行えます。

Ubuntuでの操作例

```console
$ sudo apt install rabbit rabbit-theme-clear-code 
$ sudo apt install fonts-motoya-l-maruberi
$ cd lts-release-slide
$ rake # スライド表示
$ rake pdf # PDF生成
```
